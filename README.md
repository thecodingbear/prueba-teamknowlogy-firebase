# Prueba Jonathan Schell Teamknowlogy - Firebase

### Objetivo
Template de correo electrónico para Yahoo Mail, Outlook y Gmail con NodeJS y
servicio en la nube

### Solución aplicada:
Cree un servicio en la nube de Google Firebase utilizando Functions, Database y
Storage, programé en NodeJS con Nodemailer y SMTP utilizando mi propio correo
electrónico como credenciales (dichas credenciales están guardadas en la base de datos).

Como parte del cuerpo del email enviado agregué el HTML otorgado editando
algunas propiedades para legar al mejor resultado posible.

### Datos técnicos:

 - Git https://bitbucket.org/thecodingbear/prueba-teamknology-firebase/src/master/
 - Node Version : 14.3.0
 - Endpoint SendMail : https://us-central1-pruebateamknowlogy.cloudfunctions.net/sendEmail
 - Servicio de envío : SMTP – Google Gmail

### Cómo disparar un correo electrónico mediante el uso del endpoint
Se debe realizar una petición GET a https://us-central1-prueba-teamknowlogy.cloudfunctions.net/sendEmail enviando como parámetro query un correlo electrónico válido, por ejemplo

GET https://us-central1-prueba-teamknowlogy.cloudfunctions.net/sendEmail?email=email_valido_aqui