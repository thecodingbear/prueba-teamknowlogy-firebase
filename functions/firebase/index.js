const admin = require("firebase-admin");

const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://prueba-teamknowlogy.firebaseio.com"
});

const db = admin.firestore()
const storage = admin.storage()

module.exports = {
    db,
    storage
}