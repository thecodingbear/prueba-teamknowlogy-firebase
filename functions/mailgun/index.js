const nodemailer = require("nodemailer");
const template = require('./template')
const {db} = require('../firebase')

const send = (email) => {

    return new Promise((resolve, reject) => {

        const credentials = {}

        db.collection('credentials').get().then(snapshot => {

            // eslint-disable-next-line promise/always-return
            if(snapshot.empty){
    
                // eslint-disable-next-line prefer-promise-reject-errors
                reject('Imposible recuperar las credenciales.')
        
            }else{

                snapshot.forEach(element => {

                    if(element.id === 'QI0hsD729Bw6gd9mO8eU'){
                        credentials.user = element.data().username,
                        credentials.pass = element.data().password
                    }
                    
                });
    
                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: credentials.user,
                        pass: credentials.pass
                    }
                });
            
                const data = {
                    from: 'Prueba Teamknology <hello@teamknology.com>',
                    to: email,
                    subject: 'Este email es de la prueba de Jonathan Schell.',
                    html : template.template()
                };
            
                transporter.sendMail(data, (error, info) => {
                    if (error) {
                        reject(error)
                    } else {
                        resolve('El email fue enviado.')
                    }
                });
        
            }

        }).catch(err => {
            reject(err)
        })

    })

}

module.exports = {
    send
}
