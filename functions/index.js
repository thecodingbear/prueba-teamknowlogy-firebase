const cors = require('cors')({origin: true});
const functions = require('firebase-functions');
const mailgun = require('./mailgun')

exports.sendEmail = functions.https.onRequest((request, response) => {

    cors(request, response, () => {

        // GET EMAIL TO SEND EMAIL
        if(request.query.email){

            const email = request.query.email

            // eslint-disable-next-line promise/always-return
            mailgun.send(email).then(res => {
                response.status(200).send(res);
            }).catch(err => {
                console.error(err)
                response.status(400).send(err);
            })

        }else{
            response.status(400).send('Le faltó enviar un email de destino.');
        }

    })

});
